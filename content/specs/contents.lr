_model: specs-page
---
title: Spécifications fonctionnelles
---
allow_comments: no
---
body:



# Introduction

Le but de ce document est d'établir des spécifications fonctionnelles
pour un logiciel libre de gestion d'une petite librairie indépendante.
C'est à dire de décrire le fonctionnement d'un tel logiciel dans un
langage compréhensible pour des libraires. Nous partons de cas
d'utilisations d'une librairie associative de Grenoble. L'idée est de
partager ce document avec d'autres librairies pour aboutir à un logiciel
commun.

Autres spécifications
===

Pour voir un peu ce que font les autres et ce que recommandent les
professionnels, voir par exemple :

-   pages 13 et 14 de
    <http://ecla.aquitaine.fr/content/download/76295/596119/file/CREATION_REPRISE.pdf>
-   <http://www.alire.asso.fr/lettralire/2011tableauSSII.pdf>

Glossaire
===

Le but de cette section est de préciser les termes employés par la suite
dans ce document. On les regroupe en trois catégories :

### Référentiel

Le **référentiel** regroupe tout ce que ne décide pas la librairie :
description des livres, des films, les diffuseurs, les éditions, etc.

#### Clients

Il peut être nécessaire de définir des clients.

* il y a les clients faisant régulièrement affaire avec la librairie, pour lesquels le libraire veut garder une trace des opérations effectuées.
* parmi ceux-là, il peut y avoir des sociétaires. Ils cotisent un montant fixe à la librairie (souvent mensuellement, afin de garantir un revenu fixe). Leur achats de livres leurs sont déduits du montant cotisé, avec en plus une réduction.
* le reste est un client « anonyme », qui représente l'ensemble des clients « classiques »
d'une librairie.

#### Diffuseur

C'est l'entité qui, vis à vis de la librairie diffuse des exemplaires
(un éditeur peut donc également être diffuseur).

#### Éditeur

C'est l'éditeur du livre, cd, du tschirt au sens commun du terme.

#### Étiquette ( Groupe / Libélé / Libellule )

Les fiches peuvent être regroupées par libellule qui correspondrait par
exemple à des thématiques.

#### Fiche

Ensemble des éléments descriptifs d'un livre, d'un cd, d'un t-shirt,
etc.

La manière de stocker ces informations est interopérable avec les
formats
[MARC](http://www.bnf.fr/fr/professionnels/catalogage_indexation.html).

### Stock

Le **stock** regroupe ce qui permet de désigner l'état actuel de la
librairie : lieu, nombre d'exemplaires par lieu, etc.

#### Dépôt

Un dépôt est un ensemble de fiches avec des quantités dont les
exemplaires correspondants sont mis à disposition par un diffuseur. Les
exemplaires ne sont facturés que s'ils sont vendus. Les autres sont
retournés aux diffuseur.

Il existe un type particulier de dépôt appelé *dépôt automatique*. Les
exemplaires vendus sont automatiquement recommandés.

Régulièrement, le libraire fait des états de dépôt avec les diffuseurs
concernés,

#### Exemplaire

C'est une instance physique d'une fiche : quelque chose qu'on peut tenir
dans les mains.

Attention, des exemplaires d'une même fiche peuvent être diffusés par
différents diffuseurs.

Si plusieurs exemplaires d'une même fiche proviennent de plusieurs
diffuseurs, on ne peut pas savoir à quel diffuseur correspond
l'exemplaire. C'est un choix qui sera fait quand l'exemplaire est sorti
du stock.

Exemple : Trois livres identiques posés sur une table correspondent à
une fiche, trois exemplaires de cette fiche, et peuvent provenir d'un ou
plusieurs diffuseurs.

##### Exemplaire manquant

Un manquant est un exemplaire qui n'a pas pu être inventorié. Il est
marqué comme manquant jusqu'à ce qu'il soit retrouvé, déclaré perdu,
etc.

#### Lieu

Un lieu est un espace dans lequel des exemplaires sont stockés. Cela
peut être l'endroit où les exemplaires sont vendus mais également une
réserve, ou des espaces temporaires comme des stands.

##### Point de vente

C'est un lieu dans lequel des exemplaires peuvent être vendus à des
clients.

##### Réserve

C'est un lieu dans lequel les exemplaires ne sont pas vendus à des
clients, mais en attente d'être amenés sur un lieu de vente, retournés
au distributeur, ou autre.

#### Rayon

Un rayon est lié à un lieu et à un ensemble de fiche. C'est une
sous-séparartion d'un lieu.

#### Stand

On prend des cartons de livres, à vendre dans un autre lieu temporaire.
Parfois appelé « salons ».

#### Stock

L'ensemble des lieux.

### Historique

L' **historique** permet de savoir ce qu'il s'est passé, de garder des
traces de telle ou telle opérations.

#### Commande

Un bon de commande est un document qui permet de signifier à un
diffuseur des exemplaires que la librairie souhaite acquérir. Il
contient, pour chaque fiche, le titre, l'auteur, l'éditeur, l'ISBN
(potentiellement sous forme de code barre), et peut être imprimé,
consulté ultérieurement, etc.

#### Déplacement de stocks

Un déplacement de stocks est un déplacement d'exemplaires entre deux
lieux de la librairie (point de vente et réserve par exemple).

#### État de dépôt

Un état de dépôt est un document de liaison avec un diffuseur permettant
du communiquer sur l'état un dépôt. Le libraire et le diffuseur éditent
leur état de dépôt, les confrontent, et corrigent les erreurs.

#### Inventaire

Un inventaire est l'opération qui consiste à vérifier que les quantités
d'exemplaires stockés dans un lieu correspondent bien à ce qui est
attendu.

#### Mouvement

Un mouvement est une opération qui modifie le stock. Cela peut être une
sortie (vente, retour, perte, cadeau), une entrée (arrivée d'un colis,
don) ou un déplacement entre deux lieux.

#### Réassort

Le réassort est la commande de livres déjà vendus.

### Concepts

#### Alerte

Ce sont des questions à se poser, dont la résolution est différée du
moment où elles sont posées.

Par exemple : lors de la vente d'un livre dont le seuil minimal marqué
est deux, le logiciel prévient le libraire qu'il faudrait recommander.
Mais la décision de recommander ou non n'est pas faite devant le client,
mais est reportée à plus tard.

#### Listes de notices

Une liste peut contenir des exemplaires présents ou non dans le stock,
et modifier une liste ne correspond pas à des opérations physiques sur
les exemplaires du stock.

On peut appliquer des actions sur les notices d'une liste. Par
exemple, tout marquer à commander, ou juste une sélection. Ou encore,
exporter une liste en format pdf, txt ou csv.

Fonctionnalités
===

### Manipulation (physique) des stocks

#### Gestion des lieux

À la connexion, l'utilisateur sélectionne le lieu dans lequel il se
trouve pour faire ses manipulations.

#### Visualisation de l'historique

Il est possible de visualiser l'ensemble des mouvements sur le stock
sous la forme d'une liste dont chaque ligne contient :

-   date
-   Type de mouvement (entée, sortie, déplacement) ou sous-type
-   description du mouvement (lieu de vente, les deux lieux pour un
    déplacement, etc.)
-   sous liste de fiches
    -   edition
    -   titre
    -   quantité

#### Commandes

Pour tout livre, on peut définir un seuil (nombre minimum d'exemplaires
à avoir en stock), en dessous duquel une alerte est levée. Ce seuil peut
être par lieu, et global.

On peut faire une commande sans que le logiciel soit au courant (pour
les nouveautés notamment).

##### À commander

N'importe quand, l'utilisateur peut marquer des fiches comme « à
commander » (en précisant si nécessaire une quantité).

On utilise donc une liste spéciale « À commander ». Cette liste est
automatiquement mise à jour selon le seuil du nombre d'exemplaires
minimum de chaque fiche.

##### Édition de la commande

Lors de la préparation d'une commande on choisi un diffuseur, et on
poura manipuler la liste des « à commander » pour ce diffuseur
(ajouter, supprimer des exemplaires).

On valide, et cela fait un bon de commande, qui est transmis sous forme
téléphonique, électronique, papier au diffuseur.

On peut imprimer la commande à tout moment. À un moment donné, elle est
finalisée, il n'est alors plus possible de la modifier (sauf pour
marquer des exemplaires comme n'arrivant jamais).

On peut marquer certains exemplaires comme n'arrivant jamais (rupture de
stock, etc.). Ils ne sont alors plus comptés comme commandés.

##### Réponse du fournisseur

Les fiches sont marquées suivant l'état de la commande :

-   commandée (doit être livré à un moment ou un autre) ;
-   noté (commandé mais manquant : sera inclus dans le prochain colis)
-   manquant sans date (à recommander)
-   épuisé
-   etc.

##### Arrivée du colis

Les livres commandés sont marqués comme en stock.\
On réceptionne informatiquement : on ouvre une fiche réception, on bippe
les livres, on vérifie leur montant et la remise et à la fin, on vérifie
s'il y a des manquants. Si c'est le cas, on fait une réclamation.

#### Entrées

##### Arrivée d'un colis

On ouvre le colis, on a les livres en main + une facture/ou bon de
livraison pour pointer/vérifier le colis

-   en premier : remplir une fiche-colis
    -   Type de colis : dépôt, dépôt fixe, achat ferme, don
    -   date d'arrivée colis
    -   diffuseur
    -   n°de colis (n°de facture pour de l'achat ou n°de bon de
        livraison pour du dépot)
    -   pour achat facturé : reglement immédiat (à reception) ou
        échéance (avec date d'échéance)
    -   montant total de la facture (attention différent de la somme des
        prix des livres car réduction libraire + frais de port)
-   ensuite on bippe le colis (ISBN de chaque exemplaire, ou autres
    exemplaires)
    -   s'il a déjà été en stock : les informations déjà connues sont
        réutilisées
    -   si l'ISBN est inconnu : remplir une fiche avec (au moins) les
        informations (édition, titre, prix de vente, libélé). Cette
        fiche respectera un standard de l'industrie (MARC). Il est
        possible de remplir cette fiche « à la main » ou d'aller
        chercher les informations sur Internet (BNF dans un premier
        temps, en gardant une porte ouverte pour Dilicom, ou d'autres
        services).
    -   à chaque livre il faut lui designer un lieu physique où il est
        stocké : local, réserve et un rayon (avec la possibilité de
        créer un rayon)
        -   un lieu est proposé : par défaut le lieu de réception ou le
            lieu principal de vente s'il n'est plus présent (avec la
            possibilité de modifier)
        -   un rayon
-   Il est possible de mettre des commentaires sur les exemplaires et
    les colis : pour un livre manquant, un défectueux, un service de
    presse, un don, etc.

Les [avis
d'expédition](http://www.alire.asso.fr/index.php/messages-edi/avis-dexpedition)
ne sont pas gérés.

##### Don

Il s'agit d'exemplaires qui sont donnés à la librairie.

##### Entrée irrégulière

Il est possible d'ajouter un exemplaire arrivé là « par intervention
divine » pour corriger les erreurs (un livre marqué comme perdu puis
retrouvé, par exemple).

#### Sorties

Il y a plusieurs type de sorties du stock possibles :

-   vente
-   perte
-   cadeau
-   sortie irrégulière

Et des sous-type que l'on pourrait crée à souhait.

##### Départ d'un colis

-   Un retour est un colis lié à un diffuseur, à une date et une liste
    d'exemplaires.
-   La liste est constituée en bippant (par ISBN) les exemplaires, ou en
    effectuant une recherche.
-   La liste doit pouvoir être imprimée pour la placer dans le colis. Il
    faut pouvoir éditer certaines choses dans ce document : champs
    libre, modifier l'adresse… Des blancs seront laissés, et pourront
    être remplis de manière électronique.
-   Le stock est diminué d'autant. Si un exemplaire est présent dans
    plusieurs lieux, le logiciel demande de quel lieu vient l'exemplaire
    que l'on renvoie.
-   L'historique des retours est conservé afin de pouvoir le consulter.

Pour les retours, il est bien pratique d'avoir une fonction de recherche
par critères, comme "Afficher es livres du distributeur X pas vendus
depuis 12 mois", ou "les nouveautés arrivées il y a plus de 6 mois et
qui sont encore en rayon", etc.

##### La vente

###### Remarques générales

Ce que nous appellerons « vente » dans la suite est la vente d'une liste
d'exemplaires, et non pas la vente d'un exemplaire (bien qu'il soit
évidemment possible de vendre une liste d'un unique exemplaire).

Notons que pour certaines librairies associatives ne disposant pas
d'ordinateur en caisse, cette vente est faite en fin de journée à partir
d'une feuille manuscrite, donc sans ISBN à disposition

Pour les librairies se servant du logiciel en caisse, une vente est
l'ensemble des exemplaires payés en une fois par un client.

###### Actions lors de la vente

Les exemplaires vendus sont sélectionnés dans une liste, en les cochant,
en entrant l'ISBN, en cherchant le nom du livre ou de l'auteur, et ainsi
de suite.\
Le stock des exemplaires correspondant est diminué en conséquence, et la
date de vente est mémorisée.

Cas particulier : vente d'un exemplaire disponible en plusieurs modes de
dépôt différent. Quand est faite la décision ? Garder cette question
dans une liste de choix à faire.

Cas particulier : vente d'un exemplaire de plusieurs diffuseurs : il
faut décider de quel diffuseur provient l'exemplaire vendu. Cette
décision est reportée le plus tard possible. Elle doit être prise au
moment d'un retour ou d'un état de dépôt qui comprend des fiches dont un
exemplaire existe déjà dans le stock. On peut imaginer une liste de
décisions à prendre qui s'afficherait quelque part et que l'on pourrait
prendre à tout moment.

Cas particulier : si on vend des livres depuis la réserve, une alerte
est levée.

Si le livre n'est plus disponible dans le point de vente, mais qu'il
l'est en réserve, une alerte est levée.

###### Factures

À partir d'un modèle.

Cela permet également d'avoir un récapitulatif des ventes par acheteur.

###### Stock négatif

Il doit être possible de vendre un exemplaire même si le logiciel dit
qu'il n'exite pas. Une alerte sera alors stockées pour être résolue plus
tard.

##### Perte

Il s'agit d'exemplaires qui sont déclarés perdus (vol, abimé, , etc.)

##### Don

Il s'agit d'exemplaires qui sont donnés par des gens (auteurs, par
exemple) à la librairie.

##### Sortie irrégulière

Pour corriger des erreurs.

#### Déplacement de stock

Il est possible de sélectionner par pointage, recherche ou choix dans
une liste un certain nombre d'exemplaires et de les déplacer d'un lieu
à un autre.

#### Gestion des stands

##### Travail amont

Possibilité de marquer un exemplaire comme faisant souvent parti de
stand et de retrouver cette liste lors de la préparation d'un stand.

Il est possible de se constituer une base de stands. Ce sont les
exemplaires que l'on souhaite d'habitude emmener à chaque stand.

Il est également possible de constituer des « stands types », qui sont
des listes de notices à emmener sur des types de stands qui reviennent
souvent (marché de Noël, jeunesse, etc.).

##### Préparation

Le stand est créé sur le logiciel. Des informations pour le repérer
peuvent (doivent ?) être ajoutées : lieu, date, commentaires, etc. Le
stand devra être matérialisé d'une manière ou d'une autre sur le
logiciel pour que d'autres puissent rejoindre le travail en cours.

Le stand contient la base de stand (cf partie précédente), et peut
être fait à partir d'un stand type. Il est également possible d'y
ajouter une liste de notices existante ou des exemplaires
quelconques. La liste constituée jusqu'ici ne correspond pas au stand
« physique » (les livres dans les cartons) mais une liste théorique.

Une liste d'exemplaires emmenés sur le stand est constituée. Ces
exemplaires peuvent être pointés par code barre/ISBN, ou par une
recherche manuelle (pour les livres sans ISBN, les cas où on n'a pas de
douchette, ou le livre n'a pas d'ISBN, ou pour autre chose que des
livres). Les fonctions « avancées » de recherche (par lieu, par rayon,
par édition, par auteur, etc.) doivent être disponibles à ce moment là.
Si les exemplaires pointés font partie de la liste constituée au
paragraphe précédent, ils sont marqués sur cette liste comme présents
dans le stand. Sinon, ils sont ajoutés au stand sans que cette liste
soit modifiée.

Le logiciel fournit une liste des exemplaires emmenés sur le stand,
qu'il est possible d'imprimer. Cette liste a affecté un code barre à
chaque élément (y compris ceux n'ayant pas d'ISBN) ; ce code barre n'a
pas forcément de lien avec l'ISBN. Cette liste est destinée à être
imprimée et sera utile sur le stand pour marquer les ventes, et lors du
retour pour clore le stand.

Annulation : pendant cette phase, il est possible d'ajouter et d'enlever
des éléments à la liste, ce qui permet d'hésiter dans le choix des
livres.

##### Stand en lui même

Durant le stand, la liste papier crée au point précédent est utilisée
pour marquer les ventes (ceci est donc fait sans ordinateur).

À tout moment, il doit être possible de ramener des bouquins sur le
stand ou de prendre en compte une vente sur le stand.

##### Retour

Au retour, la liste papier est utilisée pour comptabiliser les ventes.

Les livres (ou autres) ramenés sont marqués comme retournés, suivant une
procédure de marquage similaire à la création de la liste. Ils sont
remis dans (un des lieux de) la librairie.

Les livres marqués comme vendus sont signalés comme tels au logiciel.
Ceci peut être fait en utilisant les codes-barre de la fiche de stand,
ou en effectuant une recherche sur le titre (ou d'autres champs), ou en
sélectionnant l'exemplaire dans une liste. Pour ces exemplaires, la même
procédure de gestion de vente que pour lors d'une vente « normale » est
utilisée : levée d'alertes pour les réassort, choix du diffuseur si
diffuseur multiple, etc.

Il est possible de marquer des livres comme donnés, etc.

Les livres restant sont marqués comme perdus.

Note : Cette procédure est très similaire à un inventaire : une fois les
livres vendus marqués comme tels, un retour de stand n'a que peu de
différences avec l'inventaire du stock de ce stand suivi d'un
déplacement de stock.

###### Lieu de retour des exemplaires

Le lieu d'origine des exemplaires sera connu. Au retour, la destination
des exemplaires sera conseillée vers le lieu d'origine, avec la
possibilité de prendre en compte immédiatement les alertes, par exemple
:\
- on est sous le seuil minimum d'un exemplaire, il vaut mieux mettre
l'exemplaire sur le lieu de vente que dans la réserve ;\
- un exemplaire a été vendu entre temps, etc.

##### Remarques diverses

-   Un travail concurrent peut être effectué pour chacune des phases :
    deux personnes ou plus peuvent faire ce travail en même temps.
-   À tout moment, les informations sur le stand (date, lieu,
    commentaire, etc.) peuvent être modifiées.
-   Des gens pourront avoir un ordinateur sur le stand pour gérer les
    ventes en direct.
-   La question d'un système pour pouvoir emmener un ordinateur pour
    gérer le stand, sans connexion à Internet, n'est pas une priorité.
-   Se souvenir des exemplaires amenés sur un stand.

#### Rayons

Dans un premier temps, il n'y aura pas de gestion des rayons.

#### Gestion des dépots

Si des alertes concernent ce diffuseur, il n'est pas possible de faire
un état de dépot : il faut régler ces alertes avant (notons que se
débarasser de cette alerte pour ce dépôt ne veut pas forcément dire la
régler : imaginons le cas où trois distributeurs sont possibles pour un
exemplaire, et où le libraire souhaite dire que le diffuseur dont il
veut faire l'état de dépôt n'est pas celui à qui a été vendu
l'exemplaire, sans pour autant prendre une décision pour les deux
autres).

Certains diffuseurs demandent à ce que des exemplaires ne restent pas
plus d'un certain temps dans un dépot. Il faut que cette contrainte soit
prise en compte au moment de l'état de dépôt.

Si une fiche a été marquée comme commandée, l'utitlisateur doit être
informé.

##### Vue de l'état des dépôts

Une liste de fiches :

-   édition
-   titre
-   auteur
-   prix
-   stock initial
-   vente
-   retour
-   réassort
-   stock final

Afficher aussi les informations concernant les autres diffuseurs.

##### État de dépôt

L'état de dépôt est un bilan fait à moment donné pour un diffuseur.

Il comporte pour chaque fiche associée au dépot de ce diffuseur :

-   stock initial
-   stock actuel
-   ventes depuis le dernier bilan
-   stock voulu
-   réassort (le nombre d'exemplaires recommandés au diffuseur)
-   retour (le nombre d'exemplaires retournés au diffuseur)

Pour les dépots automatiques le réassort est initialié avec les ventes.

#### Inventaire

##### Inventaire par lieu

Au début d'un inventaire, un lieu est marqué comme étant en cours
d'inventaire. Pendant qu'il est marqué comme tel, il ne peut pas être
modifié (pas de vente, de déplacements, etc.).

Les exemplaires (livres et autres) sont pointés les uns après les
autres, par ISBN, ou en effectuant une recherche ou en les sélectionnant
dans une liste. Ils sont alors marqués comme inventoriés. À tout moment,
il est possible de marquer un livre inventorié comme non-inventorié, et
vice-versa (pour gérer les erreurs).

L'inventaire doit être souple : à la fin de l'inventaire, on lève une
alerte pour chaque exemplaire restant. Il faudra regler le problème en
déclarant une perte, une vente etc. L'inventaire sera reèlement clos
lorsque toutes les alertes seront reglées.

Dans le cas ou plusieurs exemplaires provenant de plusieurs diffuseurs,
cela se passe de la même manière qu'une vente, et plus généralement
d'une sortie.

##### Inventaire par rayon

Idem mais avec un sous-ensemble d'un lieu, un rayon.

#### Commande client

> A part ça, il manque à mon avis (en tous cas, je ne l'ai pas vue), la
> fonction de commande client (donc commander un livre pour un client
> qui viendra le chercher dès qu'il sera dispo à la librairie).

### Visualisations et manipulation théorique

#### Visualisation des fiches/stock

Avoir une notion de fiches sans-intérêt, qui ne sont pas affichées s'il
n'y a pas de stock. C'est l'utilisateur qui décide de l'intérêt d'une
fiche, sachant qu'une fiche est intéressante la première fois qu'un
exemplaire est commandée ou entre en stock.

Avoir une vue des ventes et stocks pour décider de ce qu'on recommande
(à déplacer dans visualisation du stock ?)

Permet de voir rapidement l'état du stock. On doit pouvoir visualiser
sous forme de liste de fiches et de sous listes d'exemplaires par fiche.

-   Fiche
    -   Édition
    -   Titre
    -   Auteur
    -   Prix
    -   Diffuseurs potentiels
    -   Étiquettes
-   Stock
    -   Stock total
    -   Stock par lieu
    -   Diffuseurs du stock
-   Historique
    -   Date du derniere entrée (avec un lien vers cette entrée)
    -   Nombre d'exemplaire de la dernière entrée
    -   Total des ventes depuis dernier entrée dans le stock (avec un
        lien vers le détail de l'historique des mouvements de cette
        fiche)
    -   un commentaire éventuel
-   Permet d'effectuer des actions sur un ensemble de fiches ou
    d'exemplaires ( vente, perte, voir les détails d'une fiche, etc.).
-   Permet d'éditer/exporter/imprimer une partie de la liste

Sous-liste par exemplaire :

-   diffuseur
-   localisation
-   quantité

##### Les filtres

-   il doit possible d'afficher les fiches dont le stock est nul au lieu
    principal de vente et non nul dans un autre lieu.
-   plus généralement, des filtres fins (de type « recherche dans une
    base de donnée) sont possibles.

##### Les actions possibles :

L'idée est de pointer des exemplaires et d'effectuer des actions en
masse :

-   supprimer une fiche (en fait désactivation de la fiche pour pouvoir
    la voir dans l'historique)
-   création de listes
-   marquer à commander avec un nombre d'exemplaire, commentaire
-   ajouter une libellule
-   effectuer un mouvement
-   etc.

#### Visualisation d'une fiche

-   les infos d'une fiches
-   historique des mouvements de la fiche
-   savoir si elle est marquée a recommander
-   commentaire libre
-   etc.

Actions possibles:

-   possibilite d'éditer la fiche
-   fusionner une fiche avec une autre (en cas de doublon par exemple)
-   possibilité de la supprimer
-   marquer à commander
-   effectuer un mouvement (entrée, sortie, déplacement)
-   etc.

#### Édition de listes d'exemplaires

Possibilité de faire des listes, de les inclures dans des stands, de
les imprimer, etc.

#### Rapports et statistiques

Rien de décidé fermement pour le moment. Ça semble facilement
modifiable/ajoutable.

-   ventes du jour, du mois, de l'année ? nombre de vols ?
-   rotation par rayon : très utilisé pour savoir si le rayon tourne
    bien, si certaines grosses ventes permettent d'avoir des livres
    différents, etc.
-   connaître la part du CA par rayon et sa progression d'une année
    l'autre
-   ventilation par taux de TVA : il faut au moins qu'on puisse
    distinguer les produits à 5.5%, à 7%, à 19.6% et à 2.1%, ne
    serait-ce que pour les impôts, la paperasse officielle, etc.

#### Gestion des alertes

Dans la gestion de la librairie, il y a un certain nombres de décisions
récurentes à prendre liées à des erreurs, aux relations avec les
diffuseurs, aux inventaires, etc. Quelques exemples sont donnés
ci-après.

Il est possible de visualiser ces alertes, et de les résoudre.

##### Stock négatif

Stock négatif suite à un mouvement de sortie (peut se produire lors
d'une vente : le livre existe mais a un stock négatif ou nul)

Le stock doit repasser positif en effecutant une entree. Un type
d'entree specifique est ajouté : "entrée irrégulière" pour passer
l'éponge sur une erreur.

##### Ambiguité multi-diffuseur

Si lors d'une sortie d'un exemplaire, un autre exemplaire de la même
fiche provient d'un diffuseur différent, il faut décider quel est le
diffusieur dont l'exemplaire a été vendu.

##### Manquant

Lors d'un inventaire, des exemplaires sont constatés manquants (perdus).

Il faut effectuer une sortie ou un déplacement correspondant au nombre
d'exemplaire manquant.

#### Réassort

Fonction pouvant être utile :

> Tous les soirs (ou tous les matins, toutes les semaines), pouvoir
> afficher la liste des titres vendus depuis le dernier réassort, et
> décider s'il faut les recommander, et en combien d'exemplaires. Pour
> ça, la meilleure interface que j'aie vu se constituait d'un état de
> chaque fiche-article concernée, avec tous les détails (auteur,
> éditeur, état du stock, date de la dernière entrée, quantité de la
> dernière commande, l'historique des ventes (un histogramme est
> toujours top), et l'utilsateur n'avait qu'à rentrer le nombre
> d'exemplaires à recommander (0 ou plus), et à valider pour passer à la
> fiche suivante. Ainsi, on pouvait faire le réassort de tous les
> produits vendus en ayant toutes les infos utiles sous les yeux. Et une
> fois l'opération terminée, les livres allaient tous dans les paniers
> de commande [liste "à commander"], en étant marqués "à commander".

### Comptabilité et trésorerie

#### Gestion de caisse

La gestion de caisse dans certaines librairies associatives n'est pas
faite en direct : les livres vendus sont marqués sur une feuille lors de
la vente, et ces ventes sont reportées sur informatique en fin de
journées.

-   Gestion de caisse pour enregistrer en direct les ventes effectuées
    et effectuer une addition (ordinateur présent en caisse).
-   Impression/édition de factures : système de numérotation propre à
    chaque librairie, plus beaucoup d'autres infos : siret, siren, etc.

Attention aux liens avec la comptabilité (voir plus bas).

#### Comptabilité et trésorerie

Rien pour le moment, mais on ne ferme pas la porte.

-   échéancier des factures diffuseur
-   caisse: modes de paiement ?
-   gestion de la TVA (indispensable selon un libraire qui nous a
    conseillé)
-   gestion de la [SOFIA](http://la-sofia.org/) pour les ventes faites
    dans l'optique d'un prêt : bibli, CDI etc.
-   différents rapports ?
-   etc ?

### Diffuseurs

#### Visualisation des diffuseurs

Une liste avec :

-   le nom du diffuseur
-   nombre de fiche et d'exemplaires en stock
-   nombre de fiche et d'exemplaires en achat ferme
-   nombre de fiche et d'exemplaires en dépot ainsi que le nombre
    d'exemplaires vendus depuis le dernier état de dépôt

Actions :

-   lien dépôt
-   lien visualisation fiche/stock (filtré par le diffuseur)
-   lien historique (filtré par le diffuseur)

#### Gestion des diffuseurs

Il est possible de « trier » les diffuseurs : on peut leur affecter une
note pour préciser ceux avec lesquels on préfère traiter.

-   Lors d'une commande, on est prévenu s'il est possible de commander
    via un diffuseur mieux noté.
-   Lors de la résolution d'une alerte "multi-diffuseur", on conseille
    le diffuseur préféré.

### Comportement du logiciel, ergonomie

#### Droits et utilisateurs

Y-a-t-il besoin de plusieurs types d'utilisateurs qui auraient des
droits différents sur le logiciel ?

Voir ça en terme de rôles. C'est bien de séparer. Pour se protéger des
erreurs ou étourderies, par forcément pour les malveillances.

Il y aura tout de même des utilisateurs pour la gestion des mots de
passe, et qui ont le droit d'utiliser certains rôles.

À tout moment, on ne peut endosser qu'un seul rôle.

#### Gestion des erreurs, retour en arrière et historique

Il y aura des erreurs, qu'il faut gérer.

Par exemple, une personne s'aperçoit qu'elle vient de commettre une
erreur, elle peut alors revenir en arrière. Mais que se passe-t-il si
entre le moment ou la personne a commit l'erreur et le moment où elle
s'en rend compte une autre action a été effectuée qui rend difficile le
retour en arrière.

Autre exemple, que faire si on veut vendre un livre, et que le logiciel
croit qu'il n'en reste plus en stock ?

L'utilisateur peut :

-   Revenir en arrière à tout instant : l'opération disparaît. Dans la
    mesure du possible.
-   Effectuer des opérations de correction : cela permet de rétablir le
    stock dans un état correcte.
-   Mieux, annuler une ancienne opération, si cela est possible.

#### Travail concurrent

Le logiciel sera multi-utilisateur. Cela signifie q'un travail
concurrent peut être effectué pour chacune des phases : deux personnes
ou plus peuvent faire ce travail en même temps.

#### Travail hors ligne

Rien pour le moment. On verra plus tard.

Y aura-t-il besoin de travailler hors ligne ? Cas concret :
arrivera-t-il qu'un ordinateur soit emmené sur un stand, sans Internet,
pour « rentrer dans le logiciel » les ventes, immédiatement ?

#### Retour visuel

Il y a un retour visuel pour toute opération, surtout celles qui
modifient l'état des stocks.

Fonctionnalités non prioritaires
===

La liste suivante contient des fonctionnalités, déjà citées dans la
partie précédente, qui ne seront pas implémentées dans l'immédiat.

-   Gestion de la comptabilité et de la trésorerie
-   Possibilité de travailler sans Internet pour les stands (la gestion
    doit donc se faire sur papier)
-   Gestion des rayons
-   Liste précise des rapports et statistiques disponibles
-   Gestion de Dilicom
-   Gestion des [EDI](http://alire.asso.fr/index.php/messages-edi)
-   Gestion des livres d'occasion

Crédit: développeurs de Ruche pour la première version.