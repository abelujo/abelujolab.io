_model: doc-page
---
title: Documentation
---
allow_comments: no
---
body:

Abelujo is a free ('libre') software whose goal is to help bookshops,
either professionals and associations, to manage their stock of books,
newspapers, CDs and other objects.

While there is many features to try out, the software is considered in
 a testing stage. If you too dreamed of such a tool, you should
 contact us. We'll check together that what we are going will suit
 your needs.

We have book sources for **german**, **french** and **spanish**
books. More will come, but if you want another one, the sooner you
tell us, the sooner you'll have it.

## Current features

What you can test so far:

- **search and add books** to your database. You can search by an
    internet search with keywords, by the isbn/ean or by an advanced search.
- you can use an usb **barcode scanner**,
- you can search, manipulate your stock and have a quick insight with interactive charts,
- manage **deposits** that you established with editors,
- do **inventories** of your shelves,
- see the **history** of what happened on your stock,
- be notified with **alerts** when a book reaches its minimum threshold
  or when you sold a book that is both in your stock and in a deposit,
- create **lists of books** and export them to other formats (pdf, csv).

## Planned features

We base our development work on solid specifications we wrote. See the [specifications](/en/specs) page.

## Technical documentation

Abelujo is free software, as in "freedom". You can grab [the
sources](https://gitlab.com/vindarel/abelujo) and read the [techincal
documentation](http://dev.abelujo.cc/) to install it yourself, and
join the development.