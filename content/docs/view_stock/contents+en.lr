_model: doc-page
---
title: View your stock
---
allow_comments: no
---
sort_key: 70
---
body:

# View and manage the stock

The page "My stock" allows to search for cards in our stock and to take some actions.

We can filter the search by criteria:

* part of the title,
* part of the authors' name,
* the type of the card,
* the publisher,
* the supplier,
* the place,
* the shelf.

As a result we get a table with the following columns:

* cover,
* title,
* price,
* author(s),
* publisher,
* shelf,
* quantity in stock,
* places with exemplaries.

We can also see the isbn and click on a title to see the details of
this card, or click on a click to see the card's details on the
vendor's website.

## Sorting the results

You can sort the results list by clicking on each column's header.

## Show and hide information

You can click on an icon to show or hide more information. Especially,
this button will hide the cover, so we can see more results at once.

## Select and act on cards

If you select some cards (with the checkbox of the last column), you
can then click on the button "Add to…" in order to add them to lists
of books.

## Set cards to command

You can select cards as explained above and add them to the special
list "to command". You can view this list with the menu "To Command".

# Quick access to a card

Sometimes, we just want a quick access to a card:

<img src="/demo-gotocard.gif" alt="Quick access to a card's information" style="width: 1000px;"/>

On that page, we can view:
- the bibliographical information
- along with information regarding the stock: how many exemplaries of this card do we have in stock ? In deposits ?
- and historical information: date of the last entry, number of sells since that date.